# ・Docker Toolboxのインストールについて
### 1.exeファイルをダウンロードする。
 [ここから](https://docs.docker.com/toolbox/overview/#ready-to-get-started)入手可能。
 真ん中ぐらいにインストールできるボタンがあるのでクリック。
### 2.インストール作業をする。
下記のサイトの「2.Dockerの起動確認」の手前まで行う。

[インストール参考サイト](https://qiita.com/maemori/items/52b1639fba4b1e68fccd)

### 3.インストールが完了したら、ユーザーのアプリケーションディレクトリの中にある「Docker Quickstart Teminal」を起動する。

場所: C:\Users\i-hat(ユーザー名)\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Docker

### 4.起動したら「Oracle VM VirtualBox」を起動する。

場所: C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Oracle VM VirtualBox

※起動後、「default」という仮想マシンが実行中なことを確認する。

***

# ・社内展開用のlaradockリポジトリの取込み
### 1.C:\Users\ユーザー名のところにvmフォルダを作成する。(エクスプローラーで)

### 2.vmフォルダ内に「larawork」フォルダを作成する。(エクスプローラーで)

### 3.先ほどインストールした「Docker Quickstart Teminal」上でvmのフォルダまで移動する。

```bash
$ cd /C/Users/ユーザー名/vm
```

### 4.下記を実行する。
```bash
$ git clone https://gitlab.media-tek.jp/Tools/laradock.git
```



